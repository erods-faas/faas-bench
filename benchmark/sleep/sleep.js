function main(params) {
    milliseconds = params.milliseconds;
    return new Promise(resolve => setTimeout(() => resolve({}), milliseconds))
}
