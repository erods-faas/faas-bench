import com.google.gson.JsonObject;

public class Hello {

  public static JsonObject main(JsonObject args) {
    JsonObject response = new JsonObject();
    response.addProperty("payload", "Hello world!");
    return response;
  }
}
