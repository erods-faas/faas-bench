const Minio = require('minio')

var client = new Minio.Client({
    endPoint: '10.1.7.1',
    port: 9000,
    useSSL: false,
    accessKey: 'ACCESS_KEY',
    secretKey: 'SECRET_KEY'
});

const BUCKET_DST = 'pre-signed';

function preSigned() {
    var activation = process.env['__OW_ACTIVATION_ID'];
    var object_dst = 'object-' + activation + '.txt';
    return client.presignedPutObject(BUCKET_DST, object_dst, 5)
        .then(url => { return { "url": url }; });
}

exports.main = preSigned;
