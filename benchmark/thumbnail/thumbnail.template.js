const fs = require('fs');
const Minio = require('minio');
const sharp = require('sharp');

const minio = new Minio.Client({
    endPoint: '10.1.7.1',
    port: 9000,
    useSSL: false,
    accessKey: 'ACCESS_KEY',
    secretKey: 'SECRET_KEY'
});

const BUCKET_SRC = "images"
const BUCKET_DST = "thumbnails"
const WIDTH = 320;
const HEIGHT = 240;
const FORMAT = 'png';

function thumbnail(params) {
    var activation = process.env['__OW_ACTIVATION_ID'];
    var object_src = params['object']
    var tmpfile = "/tmp/img-" + activation
    var object_dst = object_src + "-" + activation + "-thumbnail.png";

    return minio.fGetObject(BUCKET_SRC, object_src, tmpfile)
        .then(img => sharp(tmpfile).resize(WIDTH, HEIGHT).toFormat(FORMAT).toBuffer())
        .then(buffer => minio.putObject(BUCKET_DST, object_dst, buffer))
        .then(_ => fs.unlinkSync(tmpfile))
        .then(_ => { created: object_dst })
}

exports.main = thumbnail;
