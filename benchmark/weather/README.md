## Zip source code and create action
zip -r weather.zip *
wsk action create weather --kind nodejs:6 weather.zip 

## Invoke action
wsk action invoke -b weather --param location "Paris" --param language "fr"

## Zip source code and update action
zip -r weather.zip *
wsk action update weather --kind nodejs:6 weather.zip

