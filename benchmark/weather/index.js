const GOOGLE_APIKEY = 'AIzaSyB9r5vwt9KBC4AOhvNdhKr-bO07GGcgvCE';
const DARKSKY_APIKEY = '75e78a857256ebf6c7eb68ec29c41de2';

const geocoder = require('node-geocoder')({
    provider: 'google',
    httpAdapter: 'https',
    apiKey: GOOGLE_APIKEY,
    formatter: null
});

const DarkSky = require('dark-sky');
const darksky = new DarkSky(DARKSKY_APIKEY)
      .units('si')
      .exclude('minutely,hourly,daily,alerts,flags');

const translate = require('google-translate-api');

function weather(params) {
    var location = params.location || 'Paris';
    var language = params.language || 'fr';

    return geocoder.geocode(location)
        .then(function(geolocations) {
            geolocation = geolocations[0];

            return darksky
                .latitude(geolocation.latitude)
                .longitude(geolocation.longitude)
                .get();
        })
        .then(function(meteos) {
            var meteo = meteos.currently;
            var text = 'The sky is ' + meteo.summary + ' (The temperature is ' + meteo.temperature + '°C).';
            return translate(text, {from: 'en', to: language});
        });
}

exports.main = weather;

