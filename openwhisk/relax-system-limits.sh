# ==============================================================================
# Relax some systems limits to avoid artificial bottlenecks
# ------------------------------------------------------------------------------

BK=$(date +"%s.%N").bk

sed -i.$BK '/# custom limit/d' /etc/security/limits.conf
cat <<EOF >>/etc/security/limits.conf

root soft nofile 81920 # custom limit
root hard nofile 81920 # custom limit
EOF

sed -i.$BK '/# custom limit/d' /etc/sysctl.conf
cat <<EOF >>/etc/sysctl.conf

fs.file-max = 100000 # custom limit
net.core.somaxconn = 81920 # custom limit
net.ipv4.tcp_max_syn_backlog = 81920 # custom limit
net.ipv4.tcp_syncookies = 0 # custom limit
net.ipv4.ip_local_port_range = 15000	65535 # custom limit
net.ipv4.neigh.default.gc_thresh1 = 2048 # custom limit
net.ipv4.neigh.default.gc_thresh2 = 3072 # custom limit
net.ipv4.neigh.default.gc_thresh3 = 4096 # custom limit
EOF

cat <<EOF >/etc/docker/daemon.json
{
  "bip": "172.17.0.1/16"
}
EOF
