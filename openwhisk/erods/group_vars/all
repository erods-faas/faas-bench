env_hosts_dir: "{{ lookup('env', 'ANSIBLE_VARS') }}"
openwhisk_tmp_dir: "{{ lookup('env', 'OPENWHISK_TMP_DIR') | default('/tmp', true) }}"
config_root_dir: "{{ openwhisk_tmp_dir }}/wskconf"
whisk_logs_dir: "{{ openwhisk_tmp_dir }}/wsklogs"
invoker_use_runc: "{{ ansible_distribution != 'MacOSX' }}"

db_prefix: whisk_local_

# Auto lookup to find the db credentials
db_provider: "{{ lookup('ini', 'db_provider section=db_creds file={{ playbook_dir }}/db_local.ini') }}"
db_username: "{{ lookup('ini', 'db_username section=db_creds file={{ playbook_dir }}/db_local.ini') }}"
db_password: "{{ lookup('ini', 'db_password section=db_creds file={{ playbook_dir }}/db_local.ini') }}"
db_protocol: "{{ lookup('ini', 'db_protocol section=db_creds file={{ playbook_dir }}/db_local.ini') }}"
db_host: "{{ lookup('ini', 'db_host section=db_creds file={{ playbook_dir }}/db_local.ini') }}"
db_port: "{{ lookup('ini', 'db_port section=db_creds file={{ playbook_dir }}/db_local.ini') }}"

# API GW connection configuration
apigw_auth_user: ""
apigw_auth_pwd: ""
apigw_host_v2: "http://{{ groups['apigateway']|first }}:{{apigateway.port.api}}/v2"

# Do not use https
controller_protocol: "http"

# Set kafka configuration
kafka_topics_completed_retentionBytes: 104857600
kafka_topics_completed_retentionMS: 300000
kafka_topics_health_retentionBytes: 104857600
kafka_topics_health_retentionMS: 300000
kafka_topics_invoker_retentionBytes: 104857600
kafka_topics_invoker_retentionMS: 300000

docker_registry: "registry.gitlab.com/"
docker_image_prefix: "erods-faas/openwhisk"
docker_image_tag: "50656f8194ad8841f9b0f51b8f976ed2615e25de"

whisk_loglevel: "OFF"
jfr_tracing_enabled: true
metrics_kamon: false

host_jvm: /usr/lib/jvm/java-8-oracle
perf_jvmti: "{{ host_jvm }}/perf-jvmti"

kafka_heap: 32G
controller_heap: 128G
invoker_heap: 32G

gc_log_options: -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:/logs/gc.log
jfr_perf_options: -XX:+UnlockCommercialFeatures -XX:+FlightRecorder -XX:+PreserveFramePointer
kafka_arguments: "{{ jfr_perf_options }}"
controller_arguments: "{{ gc_log_options }} {{ jfr_perf_options }}"
invoker_arguments: "{{ gc_log_options }} {{ jfr_perf_options }}"

limit_invocations_per_minute: 2147483647
limit_invocations_concurrent: 2147483647
limit_invocations_concurrent_system: 2147483647
invoker_akka_client: true
invoker_allow_multiple_instances: true
invoker_container_proxy_pauseGrace: 12 hours
invoker_container_proxy_idleContainer: 24 hours

# Definition of the capacity of the Invoker machine
invoker_user_memory: "64 G"

activationStore_invoker_spi: "whisk.core.database.NullActivationStoreProvider"
userLogs_spi: "whisk.core.containerpool.logging.NullLogStoreProvider"

runtimes_registry: "registry.gitlab.com/"
runtimes_bypass_pull_for_local_images: false
runtimes_image_prefix: "erods-faas/openwhisk-runtimes"
runtimes_image_tag: "d9e9db881187aa5a891a1c69c15a993f366db9dc"

runtimes_manifest:
  runtimes:
    nodejs:
    - kind: nodejs:8
      image:
        prefix: "{{ runtimes_image_prefix }}"
        name: nodejs-v8
        tag: "{{ runtimes_image_tag }}"
      default: false
      deprecated: false
      attached:
        attachmentName: codefile
        attachmentType: text/plain
    - kind: nodejs-health:8
      image:
        prefix: "{{ runtimes_image_prefix }}"
        name: nodejs-v8-health
        tag: "{{ runtimes_image_tag }}"
      default: true
      deprecated: false
      attached:
        attachmentName: codefile
        attachmentType: text/plain
      stemCells:
      - count: 2
        memory: 256 MB
    - kind: nodejs-perf:8
      image:
        prefix: "{{ runtimes_image_prefix }}"
        name: nodejs-v8-perf
        tag: "{{ runtimes_image_tag }}"
      default: false
      deprecated: false
      attached:
        attachmentName: codefile
        attachmentType: text/plain
  blackboxes: []
