#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void sigusr1(int sig) {
  // Do nothing, or rather:
  // let the main falls out of sleep, due to the signal
}

int main(int argc, char *argv[]) {
  signal(SIGUSR1, sigusr1);
  sleep(12 * 60 * 60);
  return 0;
}
