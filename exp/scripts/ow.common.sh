declare -A \
  controller=([name]=controller [host]=erods-mc-3 [container]=controller0 [mainclass]=whisk.core.controller.Controller) \
       kafka=([name]=kafka      [host]=erods-mc-2 [container]=kafka0      [mainclass]=kafka.Kafka) \
     invoker=([name]=invoker    [host]=erods-mc-6 [container]=invoker0    [mainclass]=whisk.core.invoker.Invoker)

declare -a components=(${components:-controller kafka invoker})

declare -a hosts=$(for _component in ${components[@]}; do
                     declare -n component=$_component
                     echo ${component[host]}
                  done | sort | uniq)

remote_java_home() {
    local _component=$1; shift
    local -n component=$_component

    ssh ${component[host]} \
         docker exec ${component[container]} \
         env \
         | grep JAVA_HOME | cut -d= -f2
}

remote_java_pid() {
    local _component=$1; shift
    local -n component=$_component

    ssh ${component[host]} \
         docker exec ${component[container]} \
         $(remote_java_home $_component)/bin/jps -l \
         | grep ${component[mainclass]} \
         | awk '{ print $1 }'
}

