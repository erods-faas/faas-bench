#! /bin/bash -e

DIR=$(dirname $(readlink -f $0))
REMOTE_DIR=/root/.debug/ow.perf

source $DIR/ow.common.sh

setup() {
    for host in ${hosts[@]}; do
        echo "[ow.perf] $host"
        ssh $host mkdir -p $REMOTE_DIR
        scp $DIR/../perf_monitor.c $host:$REMOTE_DIR/
        ssh $host make -C $REMOTE_DIR perf_monitor
    done
}

record_one() {
    local stamp=$1; shift
    local host=$1; shift

    local remote_dir=$REMOTE_DIR/$stamp

    ssh $host mkdir -p $remote_dir
    ssh $host \
        perf record -o $remote_dir/perf.data.raw -k 1 -a "$@" -- \
        $REMOTE_DIR/perf_monitor $stamp \
        ">$remote_dir/perf.out 2>$remote_dir/perf.err &"
}

record() {
    local stamp=${1:-$(date +"%s.%N")}; shift

    for host in ${hosts[@]}; do
        record_one $stamp $host "$@" &
    done

    wait

    echo "[ow.perf] Stamp is $stamp"
}

generate_java_perf_map() {
    local stamp=$1; shift
    local host=$1; shift
    local _component=$1; shift

    local -n component=$_component

    if [ ${component[host]} == $host ]; then
        local java_pid=$(remote_java_pid $_component)
        local java_home=$(remote_java_home $_component)
        ssh ${component[host]} \
            docker exec ${component[container]} \
            $java_home/perf-map-agent/bin/create-java-perf-map.sh $java_pid dottedclass sourcepos
    fi
}

stop_one() {
    local stamp=$1; shift
    local host=$1; shift

    local remote_dir=$REMOTE_DIR/$stamp

    echo "[ow.perf@$host] Waiting for perf process to finish"
    ssh $host pkill -SIGUSR1 -fn $stamp
    ssh $host "while pgrep -fa $stamp | grep -v pgrep >/dev/null 2>&1; do sleep 3; done"

    echo "[ow.perf@$host] Injecting jit symbols"
    ssh $host perf inject -i $remote_dir/perf.data.raw -j -o $remote_dir/perf.data

    echo "[ow.perf@$host] Generating java perf map files"
    for _component in ${components[@]}; do
        generate_java_perf_map $stamp $host $_component
    done

    echo "[ow.perf@$host] Generating perf script file"
    ssh $host perf script -i $remote_dir/perf.data ">$remote_dir/perf.script"

    echo "[ow.perf@$host] Done stopping perf"
}

stop() {
    local stamp=$1; shift

    for host in ${hosts[@]}; do
        stop_one $stamp $host || true &
    done

    wait
}

collect_one() {
    local stamp=$1; shift
    local results_dir=$1; shift
    local host=$1; shift

    local remote_dir=$REMOTE_DIR/$stamp
    local local_dir=$results_dir/$host

    stop_one $stamp $host || true

    scp -r $host:$remote_dir $local_dir
    ssh $host rm -rf $remote_dir
}

collect() {
    local stamp=$1; shift
    local results_dir=~/Results/$stamp/perf

    mkdir -p $results_dir
    for host in ${hosts[@]}; do
        collect_one $stamp $results_dir $host &
    done

    wait

    echo "[ow.perf] Results in $results_dir"
}

clean() {
    for host in ${hosts[@]}; do
        echo "[ow.perf]" ssh $host rm -rf "$REMOTE_DIR/[0-9]*.[0-9]*/"
        ssh $host rm -rf "$REMOTE_DIR/[0-9]*.[0-9]*/"
    done
}

command="$1"; shift
case "$command" in
    setup)
        setup $*
        ;;
    record)
        record $*
        ;;
    stop)
        stop $*
        ;;
    collect)
        collect $*
        ;;
    clean)
        clean $*
        ;;
    *)
        echo "Unknown command '$command'"
        ;;
esac
